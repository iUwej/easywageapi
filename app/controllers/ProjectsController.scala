package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc.BodyParsers._
import play.api.libs.json.Json
import play.api.libs.json.Json._
import models._
import com.typesafe.config.ConfigFactory
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future

object ProjectsController extends Controller{

	implicit val projectsFormat=Json.format[Project]
	implicit val assignFormat=Json.format[ProjectAssignment]
	implicit val workerFormat=Json.format[Beneficiary]
	implicit val attendFormat=Json.format[Attendance]

	def test = Action{
		Ok("this might work")
	}

	def save = DBAction(parse.json){implicit rs =>
		rs.request.body.validate[Project].map{ project=> 
			val id= Projects.insert(project)
			val saved=Projects.findBy(id)
			Created(toJson(saved))
		}.getOrElse(BadRequest("Invalid Json Format"))

	}

	def list(user_id: Long) = DBAction{implicit rs=>

		

		Users.findBy(user_id).map{ user=>
			Ok(toJson(Projects.findBy(user.id.get,true)))
		}.getOrElse(NotFound("User not found"))

	}

	def assign(project_id: Long)= DBAction(parse.json){ implicit rs =>

		rs.request.body.validate[Seq[ProjectAssignment]].map{assignments =>
			
			ProjectsAssignments.insert(assignments)
			Created(toJson(ProjectsAssignments.assignees(project_id)))
		}.getOrElse(BadRequest("Invalid Json data"))

	}

	def assignees(id: Long)= DBAction{implicit rs =>

		Projects.findBy(id).map{ project=>
			Ok(toJson(ProjectsAssignments.assignees(id)))
		}.getOrElse(NotFound("Project not found"))
	}

	def fetchWorkers(user_id: Long)= DBAction{implicit rs =>

		Users.findBy(user_id).map{ user =>
			Ok(toJson(Beneficiaries.findBy(user_id,true)))
		}.getOrElse(NotFound("User not found"))
	}

	def rooster= DBAction(parse.json){implicit rs =>

		rs.request.body.validate[Seq[Attendance]].map{ attendances=>
			Attendances.insert(attendances)
			Created("Items saved successfuly")
		}.getOrElse(BadRequest("Invalid Json data"))
	}

	


}