package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc.BodyParsers._

import play.api.libs.json.Json
import play.api.libs.json.Json._
import play.api.libs.json._
import play.api.libs.json.JsValue
import models._
import com.typesafe.config.ConfigFactory
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import play.libs.Akka
import scala.concurrent.duration._


object PaymentsController extends Controller{


	val apiRoot=current.configuration.getString("api_host").get
	val defaultToken=current.configuration.getString("default_token").get
	val client_id=current.configuration.getString("client_id").get
	val apiUrl="https://android.googleapis.com/gcm/send"
	val apiKey="key=AIzaSyC9KwoU09sqQh45h5tLREIF5UpNw3a0enQ"

	//implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext
	implicit val paymentFormat=Json.format[CorporatePayment]


	def sendpay(project_id: Long,account_id:String) =DBAction{implicit rs=>
		Accounts.findBy(account_id).map{account =>
			val payments=CorporatePayments.fetchPays(project_id,account)
			val auth= "Bearer "+defaultToken
			val apiURl=apiRoot+"/corporatepayments/v1/payments"
			Akka.system.scheduler.scheduleOnce(1000.microseconds){
				Logger.info("Scheduling payments for processing")
				payments.foreach{payment=>

					val result:Future[CorporatePayment]= WS.url(apiURl).withQueryString("client_id" -> client_id).withHeaders("Authorization" -> auth ).post(toJson(payment)).map{ 
						response =>
							response.json.validate[CorporatePayment].get
					}

				result onSuccess{
					case pay=>
					Logger.info("Success making payments")
				}

				result onFailure{

					case error => Logger.error("Error sending money",error)

				}

			}
			
		}
		Ok("Payments Scheduled")

		}.getOrElse(NotFound("Account not found"))
		

	}

	def authorize=DBAction{ implicit rs=>

		val project_id=1
		val account_id="somedemo"
		Akka.system.scheduler.scheduleOnce(1000.microseconds){

			GcmTokens.fetch.foreach{token=>

				val msg="{'"+token+"',data:{'project_id':"+project_id+",'account_id':'"+account_id+"'}}"
				val message=Json.parse(msg)
				val result:Future[String]=WS.url(apiUrl).withHeaders("Authorization" -> apiKey,"Content-Type" -> "application/json").post(message).map{

					response => response.json.toString
				}

				result onSuccess {
					case item => Logger.info("Notification sent")
				}

				result onFailure{
					case error => Logger.info("Error sending Notification")
				}



			}

		}

		Ok("Notification sent")

		
	}

	
}