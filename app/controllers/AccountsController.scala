package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.json.Json._
import play.api.libs.json.JsPath
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import models._
import com.typesafe.config.ConfigFactory
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future

object AccountsController extends Controller{

	val apiRoot=current.configuration.getString("api_host").get
	val defaultToken=current.configuration.getString("default_token").get
	val client_id=current.configuration.getString("client_id").get

	implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

	implicit val accountReads: Reads[Account] =(
		(JsPath \ "id").readNullable[String] and 
		(JsPath \ "branch_name").read[String] and 
		(JsPath \ "country_iso_code").read[String] and 
		(JsPath \ "local_currency_iso_code").read[String] and 
		(JsPath \ "base_currency_balances" \ "currency").read[String] and 
		(JsPath \ "base_currency_balances" \ "opening_balance").read[Double] and 
		(JsPath \ "base_currency_balances" \ "current_balance").read[Double] and 
		(JsPath \ "base_currency_balances" \ "opening_ledger").read[Double] and 
		(JsPath \ "base_currency_balances" \ "current_ledger").read[Double] and 
		(JsPath \ "status").readNullable[String] and 
		(JsPath \ "user_id").readNullable[Long] and
		(JsPath \ "name").readNullable[String] and
		(JsPath \ "account_name").readNullable[String]
		)(Account.apply _)

	implicit val accountWrites: Writes[Account] =(
		(JsPath \ "id").writeNullable[String] and 
		(JsPath \ "branch_name").write[String] and 
		(JsPath \ "country_iso_code").write[String] and 
		(JsPath \ "local_currency_iso_code").write[String] and 
		(JsPath \ "base_currency_balances" \ "currency").write[String] and 
		(JsPath \ "base_currency_balances" \ "opening_balance").write[Double] and 
		(JsPath \ "base_currency_balances" \ "current_balance").write[Double] and 
		(JsPath \ "base_currency_balances" \ "opening_ledger").write[Double] and 
		(JsPath \ "base_currency_balances" \ "current_ledger").write[Double] and 
		(JsPath \ "status").writeNullable[String] and 
		(JsPath \ "user_id").writeNullable[Long] and
		(JsPath \ "name").writeNullable[String] and
		(JsPath \ "account_name").writeNullable[String]
		)(unlift(Account.unapply))


	def view(id: String)= DBAction{ implicit rs =>
		Accounts.findBy(id).map{account =>
			Ok(toJson(account))
		}.getOrElse(NotFound("Account not found"))
	}

	def list(user_id: Long)= DBAction{ implicit rs =>

		Logger.info("Accounts request")
		
		Users.findBy(user_id).map{user =>

			Ok(toJson(Accounts.findBy(user_id)))
		}.getOrElse(NotFound("User not found"))
	}

	def save(user_id: Long) = Action.async{ implicit rs =>
		val apiUrl =apiRoot+"/corporatepayments/v1/accounts"
		val auth= "Bearer "+defaultToken
		Logger.info(auth)
		WS.url(apiUrl).withQueryString("client_id" -> client_id).withHeaders("Authorization" -> auth).post(rs.body.asJson.get).map{response =>

			if(response.status ==200){
				Logger.info(response.json.toString)
				response.json.validate[Account].map{ account =>
					DB.withSession{implicit session  =>
						val id= Accounts.insert(account)
						Accounts.setUser(id,user_id)
						Created(toJson(account))
					}
				}.getOrElse(BadRequest("Something went wrong. Annonying right?"))
			} 
			else{
				BadRequest(response.json)
			}
		}
	}

	



}