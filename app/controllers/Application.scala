package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc.BodyParsers._
import play.api.libs.json.Json
import play.api.libs.json.Json._
import models._
import com.typesafe.config.ConfigFactory
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future

object Application extends Controller {

  /*
  *2659
  *{
  * "token": "92326ff5-56f9-4c0e-8634-605778ec41c6",
  *"username": "dressfar"
  *}
  *client_id: c42b41b0-1b53-4a47-9a4b-be2ed2dba476
  */

  //c983aca2-de57-4312-a10b-653b35dec04e
  //db0c5be4-10b5-4109-95cb-69db5cf096e3
  //ce110a8c-88cd-410f-96cd-3bca197a2197

	implicit val userFormat=Json.format[User]
  implicit val tokenFormat=Json.format[GcmToken]

  



  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def register = DBAction(parse.json){ implicit rs =>

  	rs.request.body.validate[User].map{
  		user =>
  		val id= Users.insert(user)
  		Created(toJson(Users.findBy(id)))
  	}.getOrElse(BadRequest("Invalid json data"))

  }

  def login(username: String, password: String)= DBAction{implicit rs =>
      Logger.info("Authentication: "+username+" "+password)
  	Users.findBy(username,password).map{
  		user=>
  		Ok(toJson(user))
  	}.getOrElse(NotFound("Invalid user's credentials"))

  }

  def updateToken(id: Long, token: String)= DBAction{ implicit rs =>
  		Users.setToken(id,token)
      Ok("Value updated")

  }

 def addtoken(token: String)=DBAction{implicit rs=>
      
      val id=GcmTokens.insert(GcmToken(None,Some(token)))

      Created(toJson(GcmTokens.findBy(id)))

  }



  



}