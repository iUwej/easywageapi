
import play.api._
import models._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import scala.slick.lifted.Tag
import scala.slick.jdbc.meta._
import models._
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import play.api.libs.json.Json
import play.api.libs.json.Json._



object Global extends GlobalSettings{

	override def onStart(app: Application){
		Initiliazer.init
		Initiliazer.insertBeneficiaries()


	}

	

	object Initiliazer  {

		val default_address=1L
		val apiRoot=current.configuration.getString("api_host").get
		val defaultToken=current.configuration.getString("default_token").get
		val client_id=current.configuration.getString("client_id").get

		def init = {
			
			DB.withSession{ implicit s: Session =>
				Logger.info("Testing the Init function")
				createIfNotExists(TableQuery[Users],TableQuery[Addresses],TableQuery[Beneficiaries],TableQuery[Accounts],TableQuery[Projects],TableQuery[ProjectsAssignments],
				TableQuery[Attendances],TableQuery[Payments],TableQuery[GcmTokens])
				Logger.info("Moving to the next stage")
				insertData
			}

			

			

		}

		def createIfNotExists(tables: TableQuery[_ <: Table[_]]*)(implicit session: Session) {
			  tables foreach {table => if(MTable.getTables(table.baseTableRow.tableName).list.isEmpty) table.ddl.create}
		}

		def insertBeneficiaries(){

			val  beneficiaries=Seq[Beneficiary](
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Joseph Wafula"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Julius Waweru"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Peter Wasonga"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Agnes Mindila"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Sang KipKorir"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Anne Wangui"),Some(1)),
					Beneficiary(None,Some(1),"Kenya","default","Easy Wage","Kenya","Current","KSH","1234","1234","test","P.o box 1234",Some("Michael Waititu"),Some(1))
					)

			//beneficiaries.foreach(Beneficiaries.insert _)

				//make a call to the city api here
				implicit val beneficiaryFormat=Json.format[Beneficiary]
				implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext
				val apiUrl=apiRoot+"/corporatepayments/v1/beneficiaries"
				val auth= "Bearer "+defaultToken

			beneficiaries.foreach{beneficiary =>

					val results:Future[Beneficiary]= WS.url(apiUrl).withQueryString("client_id" -> client_id).withHeaders("Authorization" -> auth ).post(toJson(beneficiary)).map{ 
						response =>
							response.json.validate[Beneficiary].get
					}

					results onSuccess{
						case item => DB.withSession{implicit se: Session =>
							 		val id=Beneficiaries.insert(item)
									Logger.info("Saving beneficiary")
									Beneficiaries.setUser(id,1)
									Logger.info("Success in creating beneficiary with id "+id)
						}

					}

					results onFailure{
						case error => Logger.error("Error initializing beneficiaries ",error)
					}



				}
			

		}

		def insertData(implicit s: Session)={
			
				if(Beneficiaries.count == 0){
				//remmber to input some password
				val user=User(Some(1),"dressfar","4ALaOJfvf",Some("92326ff5-56f9-4c0e-8634-605778ec41c6"),Some("Easy Wage"),"0714271537271","dressfar@easywage.com")
				Users.delete(1)
				Users.insert(user,true)

				Logger.info("User saved")

				val address= Address(Some(1),"254714271537",Some("Juja Kalimoni"),Some("Unaitas Way"),"Nairobi",Some("001000"),"Kenya")
				Addresses.delete(1)
				Addresses.insert(address,true)


		  	}
			
		}

		

		
		
	}
}