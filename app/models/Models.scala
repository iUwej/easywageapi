package models;

import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import scala.slick.lifted.Tag
import java.util.Date


//add organization,phone,email
//20564



case class GcmToken(id: Option[Long],device_id:Option[String])

class GcmTokens(tag: Tag) extends Table[GcmToken](tag,"GCMTOKEN"){

	def id=column[Long]("id",O.PrimaryKey,O.AutoInc)
	def device_id=column[String]("device_id",O.Nullable)

	def * =(id.?,device_id.?) <> (GcmToken.tupled, GcmToken.unapply _)
}

object GcmTokens{

	val gcmTokens= TableQuery[GcmTokens]

	def findBy(id: Long)(implicit s: Session): Option[GcmToken]={
		gcmTokens.filter(_.id === id).firstOption
	}

	def fetch(implicit s : Session): List[String]={

		gcmTokens.map(_.device_id).list
	}

	def insert(token: GcmToken)(implicit s: Session): Long={
		(gcmTokens returning gcmTokens.map(_.id)) +=token
	}

}
case class User(id: Option[Long],username: String,password: String,accessToken: Option[String],organization:Option[String],phone:String,email: String)

class Users (tag: Tag) extends Table[User](tag,"USER"){

	def id=column[Long]("id",O.PrimaryKey,O.AutoInc)
	def username=column[String]("username",O.NotNull)
	def password=column[String]("password",O.NotNull)
	def accessToken=column[String]("accessToken")
	def organization=column[String]("organization")
	def phone=column[String]("phone")
	def email=column[String]("email")

	def * =(id.?, username, password,accessToken.?,organization.?,phone,email) <> (User.tupled, User.unapply _)
}

object Users{
	val users=TableQuery[Users]

	def findBy(id: Long)(implicit s: Session): Option[User]={
		users.filter(_.id === id).firstOption
	}

	def findBy(username: String,password: String)(implicit s: Session): Option[User]={
		users.filter(_.username === username).filter(_.password === password).firstOption
	}

	def insert(user: User)(implicit s: Session): Long={
		(users returning users.map(_.id)) +=user
	}

	def insert(user: User,dummy: Boolean)(implicit s: Session): Long={
		users += user
		user.id.get
	}

	def delete(id: Long)(implicit s: Session){
		users.filter(_.id ===id).delete
	}

	def setToken(id: Long,token : String)(implicit s: Session){
		 users.filter(_.id === id).map(u => u.accessToken).update(token)

	}
}

case class Address (id: Option[Long],number: String,street:Option[String],town: Option[String],city: String,postal_code: Option[String],country: String)

class Addresses (tag: Tag) extends Table[Address](tag,"ADDRESSES"){

	def id =column[Long]("id",O.PrimaryKey,O.AutoInc)
	def number=column[String]("number",O.NotNull)
	def street=column[String]("street")
	def town=column[String]("town")
	def city=column[String]("city",O.NotNull)
	def postal_code=column[String]("postal_code")
	def country=column[String]("country",O.NotNull)

	def * = (id.?,number,street.?,town.?,city,postal_code.?,country) <> (Address.tupled, Address.unapply _)
}

object  Addresses {

	val addresses=TableQuery[Addresses]

	def findBy(id: Long)(implicit s: Session): Option[Address]={
		addresses.filter(_.id === id).firstOption
	}

	def insert(address: Address)(implicit s: Session): Long={
		(addresses returning addresses.map(_.id)) +=address
	}

	def insert(address: Address, dummy: Boolean)(implicit s: Session): Long={
		addresses +=address
		address.id.get
	}

	def delete(id: Long)(implicit s: Session)={
		addresses.filter(_.id === id).delete
	}
	
}



case class Beneficiary(id: Option[String],address_id: Option[Long],country: String, entity_type:String,
	company_name: String,bank_country: String,bank_account_type: String,currency: String,routing_code: String,bic: String,
	iban: String, bank_address: String,name: Option[String],user_id: Option[Long])

class Beneficiaries (tag: Tag) extends Table[Beneficiary](tag,"BENEFICIARY"){

	val addresses=TableQuery[Addresses]

	def id =column[String]("id",O.PrimaryKey)
	def address_id=column[Long]("address_id")
	def country=column[String]("country",O.NotNull)
	def entity_type=column[String]("entity_type",O.NotNull)
	def company_name=column[String]("company_name",O.NotNull)
	def bank_country=column[String]("bank_country",O.NotNull)
	def bank_account_type=column[String]("bank_account_type",O.NotNull)
	def currency=column[String]("currency",O.NotNull)
	def routing_code=column[String]("routing_code",O.NotNull)
	def bic=column[String]("bic",O.NotNull)
	def iban=column[String]("iban",O.NotNull)
	def bank_address=column[String]("bank_address",O.NotNull)
	def name=column[String]("name",O.Nullable)
	def user_id=column[Long]("user_id",O.Nullable)

	def * = (id.?, address_id.?,country,entity_type,company_name,bank_country,bank_account_type,currency,routing_code,bic,iban,bank_address,name.?,user_id.?) <> (Beneficiary.tupled, Beneficiary.unapply _)

	def address=foreignKey("ADDRESS",address_id,addresses)(_.id)
}

object   Beneficiaries{

	val beneficiaries= TableQuery[Beneficiaries]

	def findBy(id: String)(implicit s: Session): Option[Beneficiary]={
		beneficiaries.filter(_.id === id).firstOption
	}

	def findBy(user_id: Long, flag: Boolean)(implicit s: Session): List[Beneficiary]={
		beneficiaries.filter(_.user_id === user_id).list
	}

	def insert(beneficiary: Beneficiary)(implicit s: Session): String= {
		beneficiaries +=beneficiary
		beneficiary.id.get
	}

	def setUser(id: String,user_id: Long)(implicit s: Session){
		beneficiaries.filter(_.id === id).map(_.user_id).update(user_id)
	}

	def delete(id: String)(implicit s: Session)={
		beneficiaries.filter(_.id === id).delete
	}

	def count(implicit s: Session): Int={
		Query(beneficiaries.length).first
	}


	
}

case class CurrencyBalance(currency: String,opening_balance: Double,current_balance: Double,opening_ledger: Double,current_ledger: Double)

case class Account(id: Option[String],branch_name: String,country_iso_code: String,local_currency_iso_code: String, currency: String,opening_balance:Double
	,current_balance: Double,opening_ledger:Double,current_ledger:Double,status: Option[String],user_id: Option[Long],name: Option[String],account_name:Option[String])

class Accounts(tag: Tag) extends Table[Account](tag,"ACCOUNT"){
	def id=column[String]("id",O.PrimaryKey)
	def branch_name=column[String]("branch_name",O.NotNull)
	def country_iso_code=column[String]("country_iso_code",O.NotNull)
	def local_currency_iso_code=column[String]("local_currency_iso_code",O.NotNull)
	def currency=column[String]("currency",O.NotNull)
	def opening_balance=column[Double]("opening_balance",O.Default(0))
	def current_balance=column[Double]("current_balance",O.Default(0))
	def opening_ledger=column[Double]("opening_ledger",O.Default(0))
	def current_ledger=column[Double]("current_ledger",O.Default(0))
	def status=column[String]("status")
	def user_id=column[Long]("user_id",O.Nullable)
	def name =column[String]("name",O.Nullable)
	def account_name = column[String]("account_name",O.Nullable)

	def * = (id.?,branch_name,country_iso_code,currency,local_currency_iso_code,opening_balance,current_balance,opening_ledger,current_ledger,status.?,user_id.?,name.?,account_name.?) <> (Account.tupled, Account.unapply _)
	def base_currency_balances =(currency,opening_balance,current_balance,opening_ledger,current_ledger) <> (CurrencyBalance.tupled, CurrencyBalance.unapply _)
}

object Accounts {

	val accounts= TableQuery[Accounts]

	def findBy(id: String)(implicit s: Session): Option[Account]={
		accounts.filter(_.id === id).firstOption
	}

	def setUser(id: String,user_id: Long)(implicit s : Session){
		accounts.filter(_.id === id).map(account => account.user_id).update(user_id)
	}

	def insert(account: Account)(implicit s: Session): String={
		
		accounts +=account
		account.id.get
		
	}

	def delete(id: String)(implicit s: Session)={
		accounts.filter(_.id ===id).delete
	}

	def findBy(id: Long)(implicit s: Session) :	List[Account]={
		accounts.filter(_.user_id === id).list
	}
	
}

case class Project(id: Option[Long],name: String,description:Option[String],budget:Option[Double],start_date:Option[String],end_date:Option[String],user_id: Long)

class Projects(tag: Tag) extends Table[Project](tag,"PROJECTS"){
	def id =column[Long]("id",O.PrimaryKey,O.AutoInc)
	def name=column[String]("name",O.NotNull)
	def description=column[String]("description")
	def budget=column[Double]("budget")
	def start_date=column[String]("start_date")
	def end_date=column[String]("end_date")
	def user_id=column[Long]("user_id",O.NotNull)

	def * = (id.?, name,description.?,budget.?,start_date.?,end_date.?,user_id) <> ( Project.tupled, Project.unapply _)
}

object Projects{

	val projects = TableQuery[Projects]
	def findBy(id: Long)(implicit s: Session): Option[Project]={
		projects.filter(_.id === id).firstOption
	}

	def findBy(id: Long,user: Boolean)(implicit s: Session): List[Project]={
		projects.filter(_.user_id === id).list
	}

	def insert(project: Project)(implicit s: Session): Long={

		(projects returning projects.map(_.id)) +=project
	}

	def delete(id: Long)(implicit s: Session){
		projects.filter(_.id === id).delete
	}

}

case class ProjectAssignment(id: Option[Long],project_id: Long, role: Option[String],rate:Double,beneficiary_id:Option[String])

class ProjectsAssignments(tag: Tag) extends Table[ProjectAssignment](tag,"PROJECTSASSIGNMENTS"){

	def id = column[Long]("id",O.PrimaryKey,O.AutoInc)
	
	def project_id=column[Long]("project_id",O.NotNull)
	def role= column[String]("role")
	def rate=column[Double]("rate",O.NotNull)
	def beneficiary_id=column[String]("beneficiary_id")

	def * = (id.?,project_id,role.?,rate,beneficiary_id.?) <> (ProjectAssignment.tupled, ProjectAssignment.unapply _)

	def idx= index("idx_proj",(beneficiary_id,project_id),unique=true)
}

object ProjectsAssignments{

	val assignments= TableQuery[ProjectsAssignments]
	val beneficiaries= TableQuery[Beneficiaries]

	def findBy(id: Long)(implicit s: Session): Option[ProjectAssignment]={

		assignments.filter(_.id === id).firstOption
	}

	def insert(assignment: ProjectAssignment)(implicit s: Session): Long={
		(assignments returning assignments.map(_.id)) +=assignment
	}

	def insert(list: Seq[ProjectAssignment])(implicit s: Session){
		list.foreach{assignment =>
			assignments +=assignment
		}
	}

	def delete(id: Long)(implicit s: Session){
		assignments.filter(_.id === id).delete
	}

	def assignees(project_id: Long)(implicit s: Session): List[Beneficiary]={
		val assigns=assignments.filter(_.project_id === project_id).map(_.beneficiary_id)
		beneficiaries.filter(_.id in assigns).list

	}


}

case class Attendance(id:Option[Long],assignment_id: Long,date: String,present: Option[Boolean])

class Attendances(tag: Tag) extends Table[Attendance](tag,"ATTENDANCE"){
	def id = column[Long]("id",O.PrimaryKey,O.NotNull)
	def assignment_id=column[Long]("assignment_id",O.NotNull)
	def date =column[String]("date",O.NotNull)
	def present=column[Boolean]("present",O.Default(true))

	def * = (id.?, assignment_id, date, present.?) <> (Attendance.tupled, Attendance.unapply _)

}

object Attendances {

	val attendances = TableQuery[Attendances]

	def findBy(id: Long)(implicit s: Session): Option[Attendance]={

		attendances.filter(_.id ===id).firstOption
	}

	def insert(attendance: Attendance)(implicit s: Session): Long={
		(attendances returning attendances.map(_.id)) +=attendance
	}

	def insert(list: Seq[Attendance])(implicit s: Session){
		list.foreach{ attendance =>
			attendances +=attendance
		}
	}

	def delete(id: Long)(implicit s: Session)={
		attendances.filter(_.id === id).delete
	}
}

case class Payment(id: Option[Long],account_id: Long,project_id: Long,value_date: String,status:Option[String],user_id: Option[Long],authorized:Option[Boolean])
class Payments(tag: Tag) extends Table[Payment](tag,"PAYMENTS"){

	def id=column[Long]("id",O.PrimaryKey,O.NotNull)
	def project_id=column[Long]("assignment_id",O.NotNull)
	def account_id=column[Long]("account_id",O.NotNull)
	def value_date=column[String]("value_date",O.NotNull)
	def status=column[String]("status",O.Nullable)
	def user_id=column[Long]("user_id",O.Nullable)
	
	def authorized=column[Boolean]("authorized",O.Nullable)
	def * = (id.?,project_id,account_id,value_date,status.?,user_id.?,authorized.?) <> (Payment.tupled, Payment.unapply _)



}

object Payments{

	val payments= TableQuery[Payments]

	def findBy(id: Long)(implicit s: Session): Option[Payment]={
		payments.filter(_.id === id).firstOption
	}

	def insert(payment: Payment)(implicit s: Session): Long={
		(payments returning payments.map(_.id)) += payment
	}

	def delete(id: Long)(implicit s: Session)={
		payments.filter(_.id === id).delete
	}
}

case class CorporatePayment(branch_name: String,transaction_reference_number: String,value_date: String,email: String,payment_method:String,
	payment_currency: String,payment_amount: Double,payment_type: String,beneficiary_id:String,debit_account_name: String,debit_account_number:String,
	created_by: String,created_on: String,customer_reference_number: String)

object CorporatePayments{

	val beneficiaries= TableQuery[Beneficiaries]
	

	def fetchPays(project_id: Long,account: Account)(implicit s: Session): List[CorporatePayment]={
		
		beneficiaries.map(_.id).list.map{ id =>
			val value_date=new Date().toString
			CorporatePayment(account.branch_name,project_id.toString,value_date,"uwej@easy.com","MOBILE","KSH",500,"REGULAR",id,"ACCOUNT123",account.id.get,"user",value_date,id)
		}
	}
	


}



